let downAngle = 90;
let defaultAngle = 0;

function toggle(textId, shutterId, arrowId) {
  // if the text is shown => collapse it
  if (window.getComputedStyle(textId).maxHeight != "0px") {
    textId.style.maxHeight = "0px";
    shutterId.style.display = "block";
    arrowId.setAttribute("style", "transform: rotate(" + downAngle + "deg)");
  } else { // => unfold
    textId.style.maxHeight = "50em";
    shutterId.style.display = "none";
    arrowId.setAttribute("style", "transform: rotate(" + defaultAngle + "deg)");
  }

}
